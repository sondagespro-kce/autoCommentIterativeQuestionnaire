# LS-AutoComment_IterativeQuestionnaire
Module for Lime Survey - Creates automatic comment questions, and for iterative quesitonnaires, create a new questionnaire from a previous round questionnaire

## Installation

### Via GIT
- Go to your LimeSurvey Directory (version up to 2.06)
- Clone in plugins/autoCommentIterativeQuestionnaire directory

### Via ZIP dowload
- Get the file zip and uncompress it
- Move the files included to plugins/autoCommentIterativeQuestionnaire directory

## Home page & Copyright
- Copyright © 2014-2016 Belgian Health Care Knowledge Centre (KCE) <http://www.kce.fgov.be/>
- Copyright © 2014-2016 Denis Chenu <http://sondages.pro>
- Licence : GNU General Affero Public License <https://www.gnu.org/licenses/agpl-3.0.html>
